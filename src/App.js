import React, { Component } from 'react';
import { Grommet, Box } from 'grommet'
import ProductList from './container/ProductList';
import AppBar from './container/AppBar';
import Filters from './container/Filters';
class App extends Component {
  state = {
    filter: ''
  }
  render() {
    const {
      filter
    } = this.state;
    return (
      <Grommet plain full>
        <Box direction="column" fill>
          <AppBar />
          <Box
            direction="row"
            pad="medium"
            fill
          >
            <Box width="medium">
              <Filters onFilterChange={(filter) => this.setState({filter})}/>
            </Box>
            <Box flex>
              <ProductList filter={filter}/>
            </Box>
          </Box>
        </Box>
      </Grommet>
    );
  }
}

export default App;
