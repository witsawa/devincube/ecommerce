import React, { Component } from 'react'
import { Box, InfiniteScroll } from 'grommet'
import ProductItem from './ProductItem'
import request from '../utils/request'
import _ from 'lodash'

export default class ProductList extends Component {
  state = {
    data: [],
    totalPage: 1,
    currentPage: 1,
    totalItems: 0,
    limit: 100
  }
  componentDidMount() {
    this.fetchData()
  }
  shouldComponentUpdate(nextProps, nextState) {
    const prevState = this.state
    const prevProps = this.props
    // default react should component update not check for deep object comparision
    if (!_.isEqual(nextState, prevState)) {
      return true
    }
    if (!_.isEqual(nextProps, prevProps)) {
      return true
    }
    return false
  }
  componentDidUpdate() {
    this.fetchData()
  }
  fetchData = async () => {
    const {
      currentPage,
      limit,
    } = this.state;
    const {
      filter
    } = this.props;
    // const nextPage = currentPage + 1;
    const response = await request.get(`/products?include=main_image&&page[limit]=${limit}&page[offset]=${(currentPage-1)*limit}&&filter=${filter}`)
    const data = response.data.data.map(o => {
      let image = 'https://via.placeholder.com/300x400.png';
      if (o.relationships.main_image) {
        const fileId = o.relationships.main_image.data.id
        const file = response.data.included.main_images.find(function(el) {
          return fileId === el.id;
        });
        image = file.link.href
      }
      return {
        name: o.name,
        description: o.description,
        price: o.meta.display_price.with_tax.formatted,
        image,
      }
    })
    this.setState({
      data,
      totalPage: response.data.meta.page.total,
      currentPage: response.data.meta.page.current,
      totalItems: response.data.meta.results.total
    })
  }
  render() {
    const {
      data
    } = this.state;
    return (
      <Box
        direction="column"
        pad="small"
        fill
      >
        <Box pad="small" background="light-3" >
          Product list
        </Box>
        <Box
          pad="small"
          direction="row"
          fill
          wrap
          overflow="auto"
        >
          {
            data.map((product) => (
              <ProductItem
                name={product.name}
                description={product.description}
                price={product.price}
                image={product.image}
              />
            ))
          }
        </Box>
      </Box>
    )
  }
}
