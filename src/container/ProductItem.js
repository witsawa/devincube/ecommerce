import React, { Component } from 'react'
import { Box, Image, Heading, Text, Stack } from 'grommet';

export default class ProductItem extends Component {
  render() {
    const {
      name,
      image,
      price,
      description
    } = this.props;
    return (
      <Box
        direction="column"
        basis="medium"
        pad="small"
      >
        <Box>
          <Stack fill anchor="top-right">
            <Box height="small">
              <Image
                fit="cover"
                src={image}
              />
            </Box>
            <Box background="brand" pad="xsmall">
              <Text>{price}</Text>
            </Box>
          </Stack>
        </Box>
        <Box
          pad={{
            horizontal: 'small',
            top: 'small',
            bottom: 'medium'
          }}
        >
          <Heading textAlign="center" level={4} margin={{vertical: 'xsmall'}}>
            {name}
          </Heading>
          <Text textAlign="center">
            {description}
          </Text>
        </Box>
      </Box>
    )
  }
}
