import React, { Component } from 'react'
import { Box, TextInput, Button, FormField } from 'grommet'

export default class Filters extends Component {
  state = {
    name: '',
    categoryId: 0,
    search: '',
  }
  setValue = async (key, value, submit=true) => {
    await this.setState({
      [key]: value
    })
    if (submit) {
      this.handleFilterChange()
    }
  }
  submitSearch = () => {
    const {
      search
    } = this.state;
    this.setValue('name', search)
  }
  handleFilterChange = () => {
    const {
      onFilterChange,
    } = this.props;
    const {
      name,
      cateogryId
    } = this.state;
    const filter = []
    if(name) {
      filter.push(`like(name,*${name}*)`)
    }
    if(cateogryId) {
      filter.push(`eq(category.id,*${cateogryId}*)`)
    }
    onFilterChange(filter.join(':'))
  }
  render() {
    const {
      q,
    } = this.state;
    return (
      <Box
        direction="column"
        pad="small"
      >
        <Box
          direction="row"
          align="end"
        >
          <FormField label="Name">
            <TextInput
              plain
              placeholder="Search"
              value={q}
              onChange={event => this.setValue('search', event.target.value, false)}
            />
          </FormField>
          <Button label="Search" onClick={() => this.submitSearch()} margin={{bottom: 'small'}}/>
        </Box>
      </Box>
    )
  }
}
